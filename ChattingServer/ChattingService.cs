﻿using ChattingInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Collections.Concurrent;

namespace ChattingServer
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.Single)]
    public class ChattingService : IChattingService
    {
        public ConcurrentDictionary<string, ConnectedClient> _connectedClients = new ConcurrentDictionary<string, ConnectedClient>();

        public int Login(string username)
        {
            //is anyone else logged in with my name
            foreach(var client in _connectedClients)
            {
                if (client.Key.ToLower() == username.ToLower())
                {
                    //if yes return 1
                    return 1;
                }
                else
                    return 0;
            }

            var establishedUserConnection = OperationContext.Current.GetCallbackChannel<IClient>();

            ConnectedClient newClient = new ConnectedClient();
            newClient.connection = establishedUserConnection;
            newClient.Username = username;

            _connectedClients.TryAdd(username, newClient);

            updateHelper(0, username);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Client login: {0} at {1}", newClient.Username, System.DateTime.Now);
            Console.ResetColor();

            return 0;
        }

        public void logout()
        {
            ConnectedClient client = GetMyClient();
            if (client !=null)
            {
                ConnectedClient removedClient;
                _connectedClients.TryRemove(client.Username, out removedClient);

                updateHelper(1, removedClient.Username);

                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Client logoff: {0} at {1}", removedClient.Username, System.DateTime.Now);
                Console.ResetColor();
            }
        }

        public ConnectedClient GetMyClient()
        {
            var establishedUserConnection = OperationContext.Current.GetCallbackChannel<IClient>();
            foreach(var client in _connectedClients)
            {
                if(client.Value.connection == establishedUserConnection)
                {
                    return client.Value;
                }
            }
            return null;
        }

        public void SendMessageToAll(string message, string userName)
        {
            foreach(var client in _connectedClients)
            {
                if(client.Key.ToLower() != userName.ToLower())
                {
                    client.Value.connection.GetMessage(message, userName);
                }        
            }
        }

        private void updateHelper(int value, string userName)
        {
            foreach (var client in _connectedClients)
            {
                if (client.Value.Username.ToLower() != userName.ToLower())
                {
                    client.Value.connection.GetUpdate(value, userName);
                } 
            }
        }

        public List<string> GetCurrentUsers()
        {
            List<string> listOfUsers = new List<string>();
            foreach (var client in _connectedClients)
            {
                listOfUsers.Add(client.Value.Username);
            }

            return listOfUsers;
        }
    }
}
