﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using ChattingInterfaces;
using System.Windows;

namespace ChattingClient
{
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    class ClientCallback : IClient
    {
        public void GetMessage(string message, string username)
        {
            ((MainWindow)Application.Current.MainWindow).TakeMessage(message, username);
        }

        public void GetUpdate(int value, string userName)
        {
            switch (value)
            {
                case 0:
                    {
                        ((MainWindow)Application.Current.MainWindow).AddUserToList(userName);
                        break;
                    }

                case 1:
                    {
                        ((MainWindow)Application.Current.MainWindow).RemoveUsersFromList(userName);
                        break;
                    }
            }
        }
    }
}
